import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(
      MaterialApp(
        home: MagicBallPage(),
      ),
    );

class MagicBallPage extends StatefulWidget {
  @override
  _MagicBallPageState createState() => _MagicBallPageState();
}

class _MagicBallPageState extends State<MagicBallPage> {
  int answerBallNumber = 1;

  void renewAnswerBallState() {
    setState(() {
      this.answerBallNumber = Random().nextInt(5) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ask Me Anything'),
        backgroundColor: Colors.blueAccent.shade400,
      ),
      body: Center(
        child: Container(
          child: FlatButton(
            child: Image.asset('images/ball$answerBallNumber.png'),
            onPressed: this.renewAnswerBallState,
          ),
        ),
      ),
    );
  }
}
